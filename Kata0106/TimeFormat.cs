﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata0106
{
    public class TimeFormat
    {
        public string GetReadableTime(int secondstotal)
        {
            int hours = secondstotal / 3600;
            secondstotal -= hours * 3600;
            int minutes = secondstotal % 60;
            secondstotal -= minutes * 60;
            int seconds = secondstotal;
            return $"{hours:00}:{minutes:00}:{seconds:00}";
        }
    }
}
