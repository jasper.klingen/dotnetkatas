using Kata0106;
using System;
using Xunit;

namespace Kata0106Test
{
    public class UnitTest1
    {
        [Theory]
        //[InlineData("00:00:00", 0)]
        [InlineData("23:59:59", 86399)]
        public void GetReadableTime_InsertSeconds_ReturnReadableTimeInHHMMSSNotation(string expected, int seconds)
        {
            //Arrange
            TimeFormat tf = new();
            //Act
            string actual = tf.GetReadableTime(seconds);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
