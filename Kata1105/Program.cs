﻿using System;

namespace Kata1105
{
    class Kata1105
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a sentence:");
            string sentence = Console.ReadLine();
            PascalConverter(sentence);        
        }

        private static void PascalConverter(string v)
        {

            string minusPunctuation = "";
            foreach(char c in v)
            {
                minusPunctuation += Char.IsPunctuation(c) ? "" : c;
            }
            
            var splitted = minusPunctuation.Split(" ");
            string res = "";
            foreach( string s in splitted)
            {
                string t = s.Trim();
                if (s == "")
                    continue;
                res += t.Substring(0, 1).ToUpper() + t.Substring(1).ToLower();
            }
            Console.WriteLine(res);
            Console.ReadLine();
        }
    }
}
