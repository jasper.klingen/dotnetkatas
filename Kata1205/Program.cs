﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kata1205
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string[] str = Console.ReadLine().Split(",");
            int[] numberSet = Array.ConvertAll(str, s => int.Parse(s));
            Console.WriteLine(BiggestDifferance(numberSet));
            Console.ReadLine();
        }

        public static int BiggestDifferance(int [] numberset)
        {
            var dN = new List<int>();
            int min = numberset.Max();
            int max = numberset.Min();

            for (int i = 0; i < numberset.Length; i++)
            {
                if (numberset[i] < min)
                    min = numberset[i];
                if (numberset[i] > max)
                    max = numberset[i];
                dN.Add(max - min);
            }
            return dN.Max();
        }
    }
}
