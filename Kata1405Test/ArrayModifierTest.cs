using Kata1405;
using System;
using Xunit;

namespace Kata1405Test
{
    public class ArrayModifierTest
    {
        [Fact]
        public void Substract_string_From_Array_stringw()
        {
            //Arrange
            string[] arr = { "s", "t", "r", "i", "n", "g", "w" };
            string str = "string";
            string[] expected = { "w" };
            //Act
            string[] actual = ArrayModifier.RemoveLetters(arr, str);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Substract_balloon_From_bbllgnoaw()
        {
            //Arrange
            string[] arr = {"b", "b", "l", "l", "g", "n", "o", "a", "w"};
            string str = "balloon";
            string[] expected = {"b", "g", "w" };
            //Act
            string[] actual = ArrayModifier.RemoveLetters(arr, str);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
