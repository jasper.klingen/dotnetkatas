using System;
using Xunit;
using Kata1905;

namespace Kata1905test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(0,0)]
        [InlineData(9, 9)]

        public void SumDigProd_PassZero_ExpectZero(int expect, int actual)
        {
            //Arrange, Act & Assert
            Assert.Equal(actual, Program.sumDigProd(expect));
        }
        [Fact]
        public void SumDigProd_Pass9and8_ExpectSeven()
        {
            //Arrange, Act & Assert
            Assert.Equal(7, Program.sumDigProd(9, 8));
        }
    }

}
