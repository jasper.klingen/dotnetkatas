﻿using System;
using System.Collections.Generic;

namespace Kata2105
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
    public class Conjo
    {
        public List<string> Vowels { get; set; } = new List<string> { "a", "e", "i", "o", "u"};

        public string translateWord(string word)
        {
            string rearrangedWord = IsFirstLetterVowel(word) ? moveConsonats(word) : word;
            return rearrangedWord + "ay";
        }

        private string moveConsonats(string word)
        {

            string leadingConsonats = "";
            while (IsFirstLetterVowel(word))
            {
                if (word.Length < 1)
                    break;
                leadingConsonats += word.Substring(0, 1);
                word = word.Substring(1);
            }
            return word + leadingConsonats;
        }
        private bool IsFirstLetterVowel(string word)
        {
            return Vowels.Contains(word.Substring(0, 1));
        }

    }
}
