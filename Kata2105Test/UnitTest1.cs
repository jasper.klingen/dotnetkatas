using System;
using Xunit;
using Kata2105;

namespace Kata2105Test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("have", "avehay")]
        [InlineData("cram", "amcray")]
        [InlineData("take", "aketay")]
        [InlineData("cat", "atcay")]
        [InlineData("shrimp", "impshray")]
        [InlineData("trebuchet", "ebuchettray")]

        [InlineData("ate ", "akeyay")]
        [InlineData("apple", "appleyay")]
        [InlineData("oaken", "oakenyay")]
        [InlineData("eagle", "eagleyay")]
        public void TranslateWord_InsertWord_ExpectPigLatinTranslation(string word, string expected)
        {
            //Arrange
            Conjo program = new Conjo();
            //Act
            string actual = program.translateWord(word);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
