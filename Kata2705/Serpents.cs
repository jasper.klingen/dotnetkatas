﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata2705
{
    public class Serpents
    {
        public bool IsPerselTongue(string str)
        {
            string lowStr = str.ToLower();
            if (!lowStr.Contains('s'))
                return true;
            int consecutiveS = 0;

            for(int i = 0; i <lowStr.Length; i++)
            {
                if(lowStr[i] == 's')
                {
                    consecutiveS++;
                    continue;
                }
                if(consecutiveS == 1)
                {
                    return false;
                }
                consecutiveS = 0;
            }
            return true;
        }


    }
}
