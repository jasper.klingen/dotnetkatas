using Kata2705;
using System;
using Xunit;

namespace Kata2705Test
{
    public class UnitTest1
    {
        [Fact]
        public void IsPerselTongue_Sshessselectstoeatthatapple_True()
        {
            //Arrange
            Serpents serpents = new();
            string str = "Sshe ssselected to eat that apple.";
            bool expected = true;
            //Act
            bool actual = serpents.IsPerselTongue(str);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void IsPerselTongue_Shessselectstoeatthatapple_False()
        {
            //Arrange
            Serpents serpents = new();
            string str = "She ssselected to eat that apple.";
            bool expected = false;
            //Act
            bool actual = serpents.IsPerselTongue(str);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void IsPerselTongue_Beatricesampleslemonade_False()
        {
            //Arrange
            Serpents serpents = new();
            string str = "Beatrice samples lemonade";
            bool expected = false;
            //Act
            bool actual = serpents.IsPerselTongue(str);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void IsPerselTongue_Youssseldomsssspeakssoboldlysssomessmerizingly_True()
        {
            //Arrange
            Serpents serpents = new();
            string str = "You ssseldom sssspeak sso boldly, ssso messmerizingly.";
            bool expected = true;
            //Act
            bool actual = serpents.IsPerselTongue(str);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData("Steve likes to eat pancakes", false)]
        [InlineData("Sssteve likess to eat pancakess", true)]
        [InlineData("Beatrice samples lemonade", false)]
        [InlineData("Beatrice enjoysss lemonad", true)]

        public void IsperselTongue_Sentence_TrueOrFalse(string str, bool expected)
        {
            //Arrange
            Serpents serpents = new();
            //Act
            bool actual = serpents.IsPerselTongue(str);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
