﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata2805
{
    public class Names
    {
        public bool ValidName(string name)
        {
            string[] nameSegments = name.Split(" ");
            Dictionary<string, NameType> nameTypeSegments = new Dictionary<string, NameType>();
            if (nameSegments.Length != 2 && nameSegments.Length != 3)
                return false;
            foreach (string nameSegment in nameSegments)
            {
                if (!nameSegment.StartsWith(nameSegment.Substring(0, 1).ToUpper()))
                    return false;
                if (nameSegment.Length == 1)
                    return false;
                if (nameSegment.EndsWith('.') && !(nameSegment.Length == 2))
                    return false;
                if (nameSegment.Length == 2)
                    nameTypeSegments.Add(nameSegment, NameType.Initial);
                else
                    nameTypeSegments.Add(nameSegment, NameType.Name);
            }
            return true;
        }
    }
    public enum NameType
    {
        Name,
        Initial
    }
}
