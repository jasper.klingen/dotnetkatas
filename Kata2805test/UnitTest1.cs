using Kata2805;
using System;
using Xunit;

namespace Kata2805test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("H. Wells", true)]
        [InlineData("H. G. Wells", true)]
        [InlineData("Herbert G. Wells", true)]
        [InlineData("Herbert George Wells", true)]
        [InlineData("Herbert", false)]
        [InlineData("Herbert W. G. Wells", false)]
        [InlineData("h. Wells", false)]
        [InlineData("herbert G. Wells", false)]

        [InlineData("H Wells", false)]
        [InlineData("H. George Wells", false)]
        [InlineData("Herbert George W.", false)]
        public void ValidName_ANameWithInitials_WetherOrNotItHasTheCorrectFormat(string name, bool expected)
        {
            //Arrange
            Names names = new();
            //Act
            bool actual = names.ValidName(name);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
