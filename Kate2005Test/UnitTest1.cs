using System;
using Xunit;
using Kata2005;

namespace Kate2005Test
{
    public class UnitTest1
    {
        [Fact]
        public void Password_Insertstonk_ShouldBeInvalid()
        {
            //Arrange
            string pw = "stonk";
            string expected = "Invalid";
            //Act
            string actual = Program.PasswordChecker(pw);
            //Assert
            Assert.Equal(expected, actual);
        }
        public void Password_Insertpass_word_ShouldBeInvalid()
        {
            //Arrange
            string pw = "pass word";
            string expected = "Invalid";
            //Act
            string actual = Program.PasswordChecker(pw);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
